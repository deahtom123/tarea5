const gulp = require('gulp');
const zip = require('gulp-zip');
var fileindex = require('gulp-fileindex');

gulp.task('crear_artefacto', () =>
    gulp.src("src/*")
        .pipe(zip('webservices.zip'))
        .pipe(gulp.dest('dist'))
);


gulp.task('crear_directorio', function() {
    return gulp.src("dist/*.zip")
    .pipe(fileindex())
    .pipe(gulp.dest('dist'));
});

gulp.task('default',
  gulp.series('crear_artefacto','crear_directorio'));